package com.runnergame.game.util

import androidx.annotation.Keep

@Keep
data class RunnerSplashResponse(val url : String)