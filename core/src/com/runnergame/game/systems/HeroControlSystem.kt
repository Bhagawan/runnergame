package com.runnergame.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Rectangle
import com.runnergame.game.assets.Assets
import com.runnergame.game.components.*
import com.runnergame.game.screens.GameScreen
import kotlin.math.PI
import kotlin.math.sin

class HeroControlSystem(priority: Int): IteratingSystem(Family.all(HeroComponent::class.java, TransformComponent::class.java).get(), priority) {
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val hM = ComponentMapper.getFor(HeroComponent::class.java)
    private lateinit var obstacles : ImmutableArray<Entity>

    private var jumpFlag = false
    private var shootFlag = false
    private var addBulletFlag = false

    private var controlInterface : HeroControlInterface? = null

    private var gameState = GameScreen.STATE_RUNNING

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
         engine?.let{
            obstacles = it.getEntitiesFor(Family.all(ObstacleComponent::class.java).get())
        }
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val tC = tM.get(entity)
        val hC = hM.get(entity)
        var obstacleTC: TransformComponent?
        val heroRect = Rectangle(tC.pos.x + tC.width / 4, tC.pos.y + 10, tC.width - tC.width / 2, tC.height - 20)

        if(hC.state != HeroComponent.DEATH) {
            for(o in obstacles) {
                obstacleTC = tM.get(o)
                obstacleTC?.let {
                    val obstacleRect = Rectangle(it.pos.x, it.pos.y, it.width, it.height)
                    if(heroRect.overlaps(obstacleRect)) {
                        if(o.getComponent(WallComponent::class.java) != null) entity?.add(DynamicComponent())
                        tC.pos.y = if(o.getComponent(WallComponent::class.java) != null && hC.state == HeroComponent.JUMPING) obstacleTC.pos.y + obstacleTC.height - 50
                        else Gdx.graphics.height / 3 - 10.0f
                        hC.state = HeroComponent.DEATH
                        controlInterface?.onDeathStart()
                        entity?.add(AnimationComponent(Assets.heroDeathAnim))
                    }
                }
                if(hC.state == HeroComponent.DEATH) break
            }
        }
        if(jumpFlag && hC.state != HeroComponent.DEATH && hC.state != HeroComponent.JUMPING) {
            jumpFlag = false
            hC.currentJumpTime = 0.0f
            hC.state = HeroComponent.JUMPING
            entity?.add(AnimationComponent(Assets.heroJumpAnim))
        }
        if(shootFlag && hC.state != HeroComponent.DEATH) {
            shootFlag = false
            if(hC.bulletAmount > 0) {
                val shoot = Entity()
                shoot.add(TransformComponent(tC.pos.x + tC.width * 0.8f, tC.pos.y + tC.height * 0.45f, tC.width / 3, tC.width / 6))
                shoot.add(DrawComponent(Assets.shotAnim.getKeyFrame(Assets.shotAnim.animationDuration)))
                shoot.add(AnimationComponent(Assets.shotAnim))
                shoot.add(BulletComponent())
                shoot.add(DynamicComponent(10))
                engine.addEntity(shoot)
                hC.bulletAmount --
            }
        }
        if(addBulletFlag) {
            addBulletFlag = false
            hC.bulletAmount++
        }

        when(hC.state) {
            HeroComponent.JUMPING -> {
                hC.currentJumpTime += deltaTime
                if(hC.currentJumpTime >= hC.jumpDuration) {
                    hC.state = HeroComponent.RUNNING
                    entity?.add(AnimationComponent(Assets.heroRunAnim))
                    tC.pos.y = Gdx.graphics.height / 3 - 10.0f
                } else tC.pos.y = Gdx.graphics.height / 3 + 10 + (hC.jumpHeight * sin(PI * hC.currentJumpTime / hC.jumpDuration)).toFloat()
            }
            HeroComponent.DEATH -> {
                val deathAnim = entity?.getComponent(AnimationComponent::class.java)
                deathAnim?.let {
                    if(it.currentPosition >= it.animation.animationDuration) controlInterface?.onDeathEnd()
                } ?: controlInterface?.onDeathEnd()
            }
        }
    }


    fun jump() {
        jumpFlag = true
    }
    fun shoot() {
        shootFlag = true
    }

    fun addBullet() {
        addBulletFlag = true
    }

    fun setGameState(state: Int) {
        gameState = state
    }

    fun setInterface(controlInterface : HeroControlInterface) {
        this.controlInterface = controlInterface
    }

    interface HeroControlInterface {
        fun onDeathStart()
        fun onDeathEnd()
    }

}