package com.runnergame.game.systems

import com.badlogic.ashley.core.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.Align
import com.runnergame.game.RunnerGame
import com.runnergame.game.assets.Assets
import com.runnergame.game.components.HeroComponent
import com.runnergame.game.screens.GameScreen
import com.runnergame.game.util.Obstacles
import com.runnergame.game.util.Record
import com.runnergame.game.util.Records

class UISystem(private val batch: SpriteBatch, priority: Int): EntitySystem(priority), EntityListener {
    private val stage = Stage()
    private val pointsLabel = Label("0", Assets.assetManager.get("skin/skin.json", Skin::class.java))
    private val menuDialog = createMenuDialog()

    private var uiInterface: UIInterface? = null

    private var secondTimer = 0.0f
    private var points = 0
    private var killedMonsters = ArrayList<Obstacles>()
    private var hC : HeroComponent? = null

    private var gameState = GameScreen.STATE_RUNNING
    private var end = false
    private var gameEnded = false

    private val bullet = Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("ammo")

    init {
        createUI()
    }

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        hC = engine?.getEntitiesFor(Family.all(HeroComponent::class.java).get())?.get(0)?.getComponent(HeroComponent::class.java)
        engine?.addEntityListener(this)
    }

    override fun update(deltaTime: Float) {
        if(end) {
            end = false
            createEndDialog().show(stage)
        }
        if(gameState == GameScreen.STATE_RUNNING) secondTimer += deltaTime
        if(secondTimer > 0.3) {
            points += (secondTimer / 0.3f).toInt()
            secondTimer %= 0.3f
        }
            pointsLabel.setText(points)

        hC?.run {
            batch.begin()
            val d : Float = 50.0f / bullet.regionHeight
            for(n in 0 until bulletAmount) {
                if(n > 9) {
                    Assets.assetManager.get("skin/MazzardSoft.fnt", BitmapFont::class.java).draw(batch, "х${bulletAmount - 9}"
                        ,(n - 0.5f) * (bullet.regionWidth * d + 5.0f), Gdx.graphics.height - 30.0f - Gdx.graphics.height / 10.0f, 50.0f, Align.left, true )
                    break
                }
                batch.draw(bullet, n * (bullet.regionWidth * d + 5.0f), Gdx.graphics.height - 80.0f - Gdx.graphics.height / 10.0f, bullet.regionWidth * d, 50.0f )
            }
            batch.end()
        }

        stage.act(deltaTime.coerceAtMost(RunnerGame.PERFECT_DT))
        stage.draw()
    }

    override fun removedFromEngine(engine: Engine?) {
        engine?.removeEntityListener(this)
    }

    fun setInterface(i: UIInterface) {
        uiInterface = i
    }

    fun addPoints(amount: Int) {
        points += amount
    }

    fun addMonsterKill(monster: Obstacles) {
        killedMonsters.add(monster)
        points += 100
    }

    private fun createUI() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.setFillParent(true)
        stage.addActor(table)
        table.pad(20.0f)
        stage.cancelTouchFocus()

        val menuBSprite = skin.getSprite("button_exit").also {
            it.setSize(Gdx.graphics.height / 10.0f,Gdx.graphics.height / 10.0f)
        }
        val buttonPause = ImageButton(SpriteDrawable(menuBSprite))
        buttonPause.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                uiInterface?.onPause()
                menuDialog.show(stage)
            }
        })
        table.add(buttonPause).top().left().width(Gdx.graphics.height / 10.0f).height(Gdx.graphics.height / 10.0f)

        val coin = Image(Assets.coinAnim.getKeyFrame(0.0f))
        table.add(coin).expandX().right().size(50.0f, 50.0f).fill()
        table.add(pointsLabel).expandX().left().padLeft(10.0f)
        table.add().size(50.0f, 50.0f)
        table.row()

        val jumpButton = ImageButton(skin.getDrawable("button_jump")).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.onJump()
                }
            })
        }
        table.add(jumpButton).expand().bottom().left().size(Gdx.graphics.height / 5.0f, Gdx.graphics.height / 5.0f).pad(0.0f, 20.0f, 20.0f, 0.0f)
        table.add().colspan(2)
        val shootButton = ImageButton(skin.getDrawable("button_shoot"))
        shootButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                uiInterface?.onShoot()
            }
        })

        table.add(shootButton).expand().bottom().right().size(Gdx.graphics.height / 4.0f, Gdx.graphics.height / 4.0f)

    }

    private fun createMenuDialog(): Dialog {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val menu = Dialog("", skin)
        menu.pad(50.0f)
        menu.contentTable.add(Label("Пауза", Label.LabelStyle(Assets.assetManager.get("skin/MazzardSoft.fnt", BitmapFont::class.java), Color.BLACK ))).center().expand()
        menu.contentTable.row()

        val exitButton = ImageButton(SpriteDrawable(skin.getSprite("button_exit").apply { setSize(100.0f, 100.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.onExit()
                }
            })
        }
        menu.contentTable.add(exitButton).center().bottom().expandX().size(Gdx.graphics.height / 10.0f,Gdx.graphics.height / 10.0f)
        val continueButton = ImageButton(SpriteDrawable(skin.getSprite("button_contunue").apply { setSize(100.0f, 100.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    menu.hide()
                    uiInterface?.onResume()
                }
            })
        }
        menu.contentTable.add(continueButton).center().bottom().expandX().size(Gdx.graphics.height / 10.0f,Gdx.graphics.height / 10.0f)

        return menu
    }

    private fun createEndDialog(): Dialog {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val menu = Dialog("", skin)
        menu.pad(50.0f)
        menu.contentTable.add(Label("Ваш счет:", skin, "records")).expand().center().colspan(2)
        menu.contentTable.row()
        menu.contentTable.add(Label(points.toString(), skin, "records")).expand().center().colspan(2)
        menu.contentTable.row()

        val exitButton = ImageButton(SpriteDrawable(skin.getSprite("button_exit_2").apply { setSize(100.0f, 100.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.onExit()
                }
            })
        }
        menu.contentTable.add(exitButton).center().bottom().expandX().size(Gdx.graphics.height / 10.0f,Gdx.graphics.height / 10.0f)
        val restartButton = ImageButton(SpriteDrawable(skin.getSprite("button_restart").apply { setSize(100.0f, 100.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    menu.hide()
                    uiInterface?.onRestart()
                    points = 0
                    killedMonsters.clear()
                    gameEnded = false
                }
            })
        }
        menu.contentTable.add(restartButton).center().bottom().expandX().size(Gdx.graphics.height / 10.0f,Gdx.graphics.height / 10.0f)

        return menu
    }

    fun getStage(): Stage {
        return stage
    }

    interface UIInterface {
        fun onShoot()
        fun onJump()
        fun onPause()
        fun onResume()
        fun onRestart()
        fun onExit()
    }

    fun setGameState(state: Int) {
        gameState = state
    }

    fun saveResult() {
        Records.saveRecord(Record(points, killedMonsters))
    }

    fun showEndDialog() {
        if(!gameEnded) {
            gameEnded = true
            end = true
        }
    }

    override fun entityAdded(entity: Entity?) {
        engine?.getEntitiesFor(Family.all(HeroComponent::class.java).get())?.let {
            if(it.size() > 0) {
                val h = it.get(0)?.getComponent(HeroComponent::class.java)
                if(h != null) hC = h
            }
        }
    }

    override fun entityRemoved(entity: Entity?) { }

}