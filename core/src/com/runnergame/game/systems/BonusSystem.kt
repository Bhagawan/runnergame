package com.runnergame.game.systems

import com.badlogic.ashley.core.*
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.math.Rectangle
import com.runnergame.game.components.BonusComponent
import com.runnergame.game.components.HeroComponent
import com.runnergame.game.components.TransformComponent

class BonusSystem(priority: Int): IteratingSystem(Family.all(BonusComponent::class.java, TransformComponent::class.java).get(), priority), EntityListener{
    private val bM = ComponentMapper.getFor(BonusComponent::class.java)
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)

    private var heroC : HeroComponent? = null
    private var heroTC : TransformComponent? = null

    private var bonusInterface: BonusInterface? = null

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        engine?.addEntityListener(this)
    }

    override fun update(deltaTime: Float) {
        if(heroC == null) {
            engine?.getEntitiesFor(Family.all(HeroComponent::class.java, TransformComponent::class.java).get())?.let {
                if(it.size() > 0) heroC = it[0].getComponent(HeroComponent::class.java)
                if(it.size() > 0) heroTC = it[0].getComponent(TransformComponent::class.java)
            }
        }
        super.update(deltaTime)
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val bC = bM.get(entity)
        val tC = tM.get(entity)

        if(bC != null && tC != null && heroC != null && heroTC != null) {
            val heroRect = Rectangle(heroTC!!.pos.x, heroTC!!.pos.y, heroTC!!.width, heroTC!!.height)
            val bonusRect = Rectangle(tC.pos.x, tC.pos.y, tC.width, tC.height)
            if(heroRect.overlaps(bonusRect)) {
                heroC?.bulletAmount = heroC?.bulletAmount?.plus(bC.bullets)!!
                if(bC.points > 0) bonusInterface?.addPoints(bC.points)
                engine.removeEntity(entity)
            }
        }
    }

    override fun removedFromEngine(engine: Engine?) {
        engine?.removeEntityListener(this)
    }

    override fun entityAdded(entity: Entity?) {
        engine?.getEntitiesFor(Family.all(HeroComponent::class.java, TransformComponent::class.java).get())?.let {
            if(it.size() > 0) heroC = it[0].getComponent(HeroComponent::class.java)
            if(it.size() > 0) heroTC = it[0].getComponent(TransformComponent::class.java)
        }
    }

    override fun entityRemoved(entity: Entity?) { }

    fun setInterface(bonusInterface: BonusInterface) {
        this.bonusInterface = bonusInterface
    }

    interface BonusInterface {
        fun addPoints(amount: Int)
    }
}