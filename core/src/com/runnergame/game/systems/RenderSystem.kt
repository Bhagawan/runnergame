package com.runnergame.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.badlogic.gdx.utils.Array
import com.runnergame.game.assets.Assets
import com.runnergame.game.components.AnimationComponent
import com.runnergame.game.components.DrawComponent
import com.runnergame.game.components.TransformComponent
import com.runnergame.game.components.WallComponent
import com.runnergame.game.screens.GameScreen
import kotlin.math.sign

class RenderSystem(private val bath: SpriteBatch, priority: Int):
    IteratingSystem(Family.all(DrawComponent::class.java, TransformComponent::class.java).get(), priority) {

    private val dM = ComponentMapper.getFor(DrawComponent::class.java)
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val aM = ComponentMapper.getFor(AnimationComponent::class.java)

    private var walls: ImmutableArray<Entity>? = null
    private val wallTop : Sprite
    private val wallBody: TiledDrawable

    private val queue = Array<Entity>()

    private val comparator =
        Comparator<Entity> { p0, p1 -> (tM.get(p0).pos.z - tM.get(p1).pos.z).sign.toInt() }

    private val terrain : TiledDrawable
    private val ground : Sprite
    private val groundPad :Float
    private var currTerrainOff = 0

    private var running = GameScreen.STATE_RUNNING

    init {
        val  tex = Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("terrain")
        val sprite = Sprite()
        val w = tex.regionWidth / 2
        sprite.setRegion(tex, 13, 0, 22, (1.5 * w).toInt())
        sprite.setSize(Gdx.graphics.width / 10.0f, Gdx.graphics.height / 3.0f)
        terrain = TiledDrawable(sprite)
        terrain.scale = Gdx.graphics.width / 10.0f / tex.regionWidth

        ground = Sprite()
        ground.setRegion(tex, 13, 15, 22, 24)

        groundPad = terrain.region.regionHeight * terrain.scale

//        wallTop = Sprite()
//        wallTop.setRegion(tex, 0, 0, tex.regionWidth, 15)
        wallTop = Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("spike")
        val wall = Sprite()
        wall.setRegion(tex, 0, 15, tex.regionWidth, 24)
        wallBody = TiledDrawable(wall)
        wallBody.scale = Gdx.graphics.width / 10.0f / tex.regionWidth
    }

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        engine?.let {
            walls = it.getEntitiesFor(Family.all(WallComponent::class.java).get())
        }
    }

    override fun update(deltaTime: Float) {
        super.update(deltaTime)

        queue.sort(comparator)

        var dC: DrawComponent?
        var tC: TransformComponent?
        var aC: AnimationComponent?

        bath.begin()
        for (entity in queue) {
            dC = dM.get(entity)
            tC = tM.get(entity)
            aC = aM.get(entity)
            aC?.let {
                bath.draw(aC.animation.getKeyFrame(aC.currentPosition), tC.pos.x, tC.pos.y, tC.width * 0.5f, tC.height * 0.5f,
                    tC.width , tC.height, tC.scale, tC.scale, tC.angle)
                aC.currentPosition += deltaTime
                if(aC.currentPosition >= aC.animation.animationDuration && aC.deleteOnEnd)
                    entity.remove(AnimationComponent::class.javaObjectType)
            } ?: bath.draw(
                dC.texture, tC.pos.x, tC.pos.y, tC.width * 0.5f, tC.height * 0.5f,
                tC.width, tC.height, tC.scale, tC.scale, tC.angle)
        }
        if(running == GameScreen.STATE_RUNNING) currTerrainOff = (currTerrainOff - Gdx.graphics.width / 100) % (terrain.region.regionWidth * terrain.scale).toInt()
        terrain.draw(bath, currTerrainOff.toFloat(),Gdx.graphics.height / 3.0f - groundPad, Gdx.graphics.width - currTerrainOff.toFloat(), groundPad)
        bath.draw(ground, 0.0f,0.0f, Gdx.graphics.width.toFloat(), Gdx.graphics.height / 3.0f - groundPad)

        var wTC: TransformComponent
        walls?.run {
            for(wall in this) {
                wTC = tM.get(wall)
                wTC.run {
                    val sc = width / wallBody.region.regionWidth
                    wallTop.scale(sc)
                    wallBody.scale = sc
                    bath.draw(wallTop, pos.x, pos.y + height - wallTop.regionHeight * sc, width, wallTop.regionHeight * sc)
                    wallBody.draw(bath, pos.x,pos.y, width,height - wallTop.regionHeight * sc)
                }
            }
        }

        bath.end()

        queue.clear()
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        entity?.let { queue.add(it) }
    }

    fun setState(running: Int) {this.running = running}
}