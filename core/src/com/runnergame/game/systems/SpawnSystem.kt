package com.runnergame.game.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.runnergame.game.assets.Assets
import com.runnergame.game.components.*
import com.runnergame.game.screens.GameScreen
import com.runnergame.game.util.Obstacles
import kotlin.random.Random

class SpawnSystem(priority: Int): EntitySystem(priority) {
    private val terrainHeight :Float = Gdx.graphics.height / 3.0f - 10
    private val heroHeight :Float = Gdx.graphics.height / 5.0f
    private val heroWidth :Float = heroHeight * 48 / 40

    private var lastObstacleTC: TransformComponent? = null
    private var lastBonusTC : TransformComponent? = null
    private var timeSinceWall = 0.0f
    private var timeSinceMonster = 0.0f

    private var difficulty = 1

    private var gameState = GameScreen.STATE_RUNNING

    override fun addedToEngine(engine: Engine?) {
        engine?.let {
            val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)

            val back = Entity()
            back.add(TransformComponent(0.0f,0.0f, -1.0f, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat()))
            back.add(DrawComponent(skin.getRegion("background")))
            it.addEntity(back)

            val hero = Entity()
            hero.add(TransformComponent(Gdx.graphics.width / 10.0f, terrainHeight, heroWidth, heroHeight))
            hero.add(DrawComponent(Assets.heroJumpAnim.getKeyFrame(0.0f)))
            hero.add(AnimationComponent(Assets.heroRunAnim))
            hero.add(HeroComponent())
            it.addEntity(hero)

        }
    }

    override fun update(deltaTime: Float) {
        if(gameState == GameScreen.STATE_RUNNING) {
            val rand = Random.nextInt(10000)
            timeSinceMonster += deltaTime * difficulty
            timeSinceWall += deltaTime * difficulty

            if(timeSinceMonster >= 0.8f && rand < 100 + timeSinceMonster * 10 * difficulty) {
                timeSinceMonster = 0.0f
                timeSinceWall = 0.0f
                spawnObstacle(Obstacles.values().drop(1).random())
            }
            else if(timeSinceWall > 0.8f && rand < 300 + timeSinceWall * 30 * difficulty) {
                timeSinceMonster = 0.0f
                timeSinceWall = 0.0f
                spawnObstacle(Obstacles.WALL)
            }
            if(Random.nextInt(1000) > 990) spawnBonus()
        }
    }

    fun setGameState( state: Int) {
        gameState = state
    }

    fun setDifficulty(newDifficulty : Int) {
        difficulty = newDifficulty
    }

    private fun spawnObstacle(obstacle: Obstacles) {
        val entity = Entity()
        entity.add(DynamicComponent())
        entity.add(ObstacleComponent())
        val scale = Gdx.graphics.width / 10.0f
        var offset = 0.0f
        lastBonusTC?.let { if(it.pos.x + it.width > Gdx.graphics.width) offset += (it.pos.x + it.width - Gdx.graphics.width).coerceAtMost(it.width) }
        lastObstacleTC?.let { if(it.pos.x + it.width > Gdx.graphics.width) offset += (it.pos.x + it.width - Gdx.graphics.width).coerceAtMost(it.width) }
        when(obstacle) {
            Obstacles.WALL -> {
                entity.add(WallComponent())
                val tC = TransformComponent(Gdx.graphics.width.toFloat() - 100 + offset, terrainHeight - 26, 100.0f, 126.0f)
                entity.add(tC)
                lastObstacleTC = tC
            }
            Obstacles.BAT -> {
                val texture = Assets.batAnim.getKeyFrame(0.0f)
                val tC = TransformComponent(Gdx.graphics.width.toFloat() - 100 + offset, terrainHeight + 310, scale, texture.regionHeight * scale / texture.regionWidth)
                entity.add(tC)
                lastObstacleTC = tC
                entity.add(DrawComponent(texture))
                entity.add(AnimationComponent(Assets.batAnim))
            }
            Obstacles.ZOMBIE -> {
                val texture = Assets.zombieAnim.getKeyFrame(0.0f)
                val tC = TransformComponent(Gdx.graphics.width.toFloat() - 100 + offset, terrainHeight - 10, scale, texture.regionHeight * scale / texture.regionWidth)
                entity.add(tC)
                lastObstacleTC = tC
                entity.add(DrawComponent(texture))
                entity.add(AnimationComponent(Assets.zombieAnim))
            }
            Obstacles.SKELETON -> {
                val texture = Assets.skeletonAnim.getKeyFrame(0.0f)
                val tC = TransformComponent(Gdx.graphics.width.toFloat() - 100 + offset, terrainHeight - 10, scale, texture.regionHeight * scale / texture.regionWidth)
                entity.add(tC)
                lastObstacleTC = tC
                entity.add(DrawComponent(texture))
                entity.add(AnimationComponent(Assets.skeletonAnim))
            }
            Obstacles.SAW -> {
                val texture = Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("saw")
                val height = texture.regionHeight * scale / texture.regionWidth
                val tC = TransformComponent(Gdx.graphics.width.toFloat() - 100 + offset, terrainHeight - 10 - height / 2, scale, height)
                entity.add(tC)
                lastObstacleTC = tC
                entity.add(DrawComponent(texture))
            }
            Obstacles.GOBLIN -> {
                val texture = Assets.goblinAnim.getKeyFrame(0.0f)
                val tC = TransformComponent(Gdx.graphics.width.toFloat() - 100 + offset, terrainHeight - 10, scale * 0.9f, texture.regionHeight * scale * 0.9f / texture.regionWidth)
                entity.add(tC)
                lastObstacleTC = tC
                entity.add(DrawComponent(texture))
                entity.add(AnimationComponent(Assets.goblinAnim))
            }
        }
        engine.addEntity(entity)
    }

    private fun spawnBonus() {
        var offset = 0.0f
        lastBonusTC?.let { if(it.pos.x + it.width > Gdx.graphics.width) offset += (it.pos.x + it.width - Gdx.graphics.width).coerceAtMost(it.width) }
        lastObstacleTC?.let { if(it.pos.x + it.width > Gdx.graphics.width) offset += (it.pos.x + it.width - Gdx.graphics.width).coerceAtMost(it.width) }
        val bonus = Entity()
        val tC = TransformComponent(Gdx.graphics.width.toFloat() + offset, terrainHeight + Random.nextInt(300), 50.0f, 50.0f )
        bonus.add(tC)
        lastObstacleTC = tC
        bonus.add(DynamicComponent())
        if(Random.nextInt(100) > 80) {
            bonus.add(BonusComponent(0, 3))
            bonus.add(DrawComponent(Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("ammo_bonus")))
        } else {
            bonus.add(BonusComponent(1000, 0))
            bonus.add(DrawComponent(Assets.coinAnim.getKeyFrame(0.0f)))
            bonus.add(AnimationComponent(Assets.coinAnim))
        }
        engine.addEntity(bonus)
    }
}