package com.runnergame.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.runnergame.game.RunnerGame
import com.runnergame.game.assets.Assets
import com.runnergame.game.components.*
import com.runnergame.game.screens.GameScreen
import com.runnergame.game.util.Obstacles

class MovingSystem(priority: Int): IteratingSystem( Family.all(TransformComponent::class.java, DynamicComponent::class.java).get(),priority) {
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val dM = ComponentMapper.getFor(DynamicComponent::class.java)
    private var movingSystemInterface: CollisionInterface? = null

    private var gameState = 0

    override fun update(deltaTime: Float) {
        if(gameState == GameScreen.STATE_RUNNING) super.update(deltaTime)
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val tC = tM.get(entity)
        val dC = dM.get(entity)
        if(tC != null &&  dC != null) {
            tC.pos.x += dC.speed
            if(entity?.getComponent(BulletComponent::class.java) != null && entity.getComponent(EffectComponent::class.java) == null) {
                val bRect = Rectangle(tC.pos.x, tC.pos.y, tC.width, tC.height)
                var oRect: Rectangle
                var oTC : TransformComponent
                for(e in entities) {
                    if(e?.getComponent(BulletComponent::class.java) == null && e?.getComponent(HeroComponent::class.java) == null && e?.getComponent(BonusComponent::class.java) == null) {
                        oTC = tM.get(e)
                        oRect = Rectangle(oTC.pos.x, oTC.pos.y, oTC.width, oTC.height)
                        if(bRect.overlaps(oRect)) {
                            if(e.getComponent(WallComponent::class.java) == null) {
                                when(e.getComponent(AnimationComponent::class.java)?.animation) {
                                    Assets.batAnim -> {
                                        movingSystemInterface?.onMonsterKill(Obstacles.BAT)
                                    }
                                    Assets.zombieAnim -> {
                                        movingSystemInterface?.onMonsterKill(Obstacles.ZOMBIE)
                                    }
                                    Assets.skeletonAnim -> {
                                        movingSystemInterface?.onMonsterKill(Obstacles.SKELETON)
                                    }
                                }
                                e.remove(AnimationComponent::class.java)
                                e.remove(ObstacleComponent::class.java)
                                oTC.pos.y = Gdx.graphics.height / 3.0f - oTC.height * 0.8f
                            }
                            entity.add(EffectComponent(tC.pos.x + tC.width / 2, tC.pos.y + tC.height / 2))
                            entity.remove(DrawComponent::class.java)
                            entity.add(DynamicComponent(-10))
                            break
                        }
                    }
                }
            }
            if(entity?.getComponent(DrawComponent::class.java)?.texture == Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("saw")) tC.angle = (tC.angle + 4) % 360
            if(tC.pos.x < -tC.width || tC.pos.x > Gdx.graphics.width + 100) engine.removeEntity(entity)
        }
    }

    fun setInterface(i: CollisionInterface) {
        movingSystemInterface = i
    }

    fun setGameState(state: Int) {
        gameState = state
    }

    interface CollisionInterface {
        fun onMonsterKill(monster: Obstacles)
    }
}