package com.runnergame.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureRegion

class AnimationComponent(var animation: Animation<TextureRegion>): Component {
    var currentPosition = 0.0f
    var deleteOnEnd = false
}