package com.runnergame.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.Gdx

class HeroComponent: Component {
    var bulletAmount = 5

    companion object {
        const val RUNNING = 0
        const val JUMPING = 1
        const val DEATH = 3
    }

    var state = RUNNING

    val jumpHeight = Gdx.graphics.height / 1.8
    val jumpDuration = 0.8f //second

    var currentJumpTime = 0.0f
}