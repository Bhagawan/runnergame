package com.runnergame.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool
import com.runnergame.game.assets.Assets

class EffectComponent(): Component {
    val effect : ParticleEffectPool.PooledEffect = Assets.effectPool.obtain()

    constructor(x: Float, y: Float): this() {
        effect.setPosition(x, y)
    }
}