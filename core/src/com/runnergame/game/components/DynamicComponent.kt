package com.runnergame.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.Gdx

class DynamicComponent(var speed: Int): Component {

    constructor(): this(-Gdx.graphics.width / 100)
}