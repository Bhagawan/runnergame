package com.runnergame.game.components

import com.badlogic.ashley.core.Component

class BonusComponent(val points: Int, val bullets: Int): Component {

}