package com.runnergame.game.screens

import com.badlogic.gdx.*
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.ScreenUtils
import com.runnergame.game.RunnerGame
import com.runnergame.game.assets.Assets

class MenuScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createMenu()
    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = GameScreen(game)
                    true
                }
                else -> false
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(inputAdapter)
        inputMultiplexer.addProcessor(stage)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(RunnerGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createMenu() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.setFillParent(true)
        table.background = skin.getDrawable("background")
        stage.addActor(table)
        table.pad(20.0f)

        val btnWidth = Gdx.graphics.width / 3.0f

        val newGameButton = TextButton("Новая Игра", skin,"menu")
        newGameButton.addListener(object : ClickListener() {
            override fun touchUp(
                event: InputEvent?,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ) {
                //Records.saveRecord(Record(100, ArrayList(listOf(Obstacles.ZOMBIE, Obstacles.SKELETON))))
                game.screen = GameScreen(game)
            }
        })
        table.add(newGameButton).size(btnWidth, 100f).expand().bottom().colspan(2)
        table.row()

        val exitButton = TextButton("Выход",skin,"menu")
        exitButton.addListener( object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                Gdx.app.exit()
            }
        })
        table.add(exitButton).size(btnWidth, 100f).expand().bottom().padBottom(50.0f).colspan(2)
        table.row()
        table.add().expandX()

        val recordButtonSize = Gdx.graphics.height / 7.0f
        val recordsButton = ImageButton(SpriteDrawable(skin.getSprite("button_records_up").apply { setSize(recordButtonSize, recordButtonSize) } ))
            .also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    game.screen = RecordsScreen(game)
                }
            })
        }
        table.add(recordsButton).size(recordButtonSize).expandX().right()
    }
}