package com.runnergame.game.screens

import com.badlogic.ashley.core.Engine
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.ScreenUtils
import com.runnergame.game.systems.*
import com.runnergame.game.util.Obstacles

class GameScreen(private val game: Game): ScreenAdapter() {
    private var batch = SpriteBatch()
    private val camera = OrthographicCamera()
    private val engine = Engine()

    companion object {
        const val STATE_RUNNING = 0
        const val  STATE_PAUSE = 1
    }

    private val controlSystem = HeroControlSystem(50).also {
        it.setInterface(object : HeroControlSystem.HeroControlInterface {
            override fun onDeathStart() {
                setGameState(STATE_PAUSE)
                saveRecord()
            }

            override fun onDeathEnd() {
                gameEnd()
            }
        })
    }
    private val uiSystem = UISystem(batch,110).also {
        it.setInterface(object: UISystem.UIInterface {
            override fun onShoot() {
                controlSystem.shoot()
            }

            override fun onJump() {
                controlSystem.jump()
            }

            override fun onPause() {
                setGameState(STATE_PAUSE)
            }

            override fun onResume() {
                setGameState(STATE_RUNNING)
            }

            override fun onRestart() {
                engine.removeAllEntities()
                engine.addSystem(SpawnSystem(1))
                setGameState(STATE_RUNNING)
            }

            override fun onExit() {
                game.screen = MenuScreen(game)
            }
        })
    }

    init {
        camera.setToOrtho(false)

        engine.addSystem(SpawnSystem(1))

        engine.addSystem(controlSystem)
        engine.addSystem(MovingSystem(60).also { it.setInterface(object : MovingSystem.CollisionInterface {
            override fun onMonsterKill(monster: Obstacles) {
                controlSystem.addBullet()
                uiSystem.addMonsterKill(monster)
            }
        }) })
        engine.addSystem(BonusSystem(51).also {
            it.setInterface(object: BonusSystem.BonusInterface {
                override fun addPoints(amount: Int) {
                    uiSystem.addPoints(amount)
                }
            })
        })
        engine.addSystem(RenderSystem(batch, 100))
        engine.addSystem(EffectSystem(batch, 101))
        engine.addSystem(uiSystem)
    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = MenuScreen(game)
                    true
                }
                Input.Keys.SPACE -> {
                    controlSystem.jump()
                    true
                }
                Input.Keys.ENTER -> {
                    controlSystem.shoot()
                    true
                }
                else -> false
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(uiSystem.getStage())
        inputMultiplexer.addProcessor(inputAdapter)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        batch.projectionMatrix = camera.combined
        camera.update()
        engine.update(delta)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    private fun setGameState(state: Int) {
        engine.getSystem(MovingSystem::class.java).setGameState(state)
        engine.getSystem(SpawnSystem::class.java).setGameState(state)
        engine.getSystem(RenderSystem::class.java).setState(state)
        controlSystem.setGameState(state)
        uiSystem.setGameState(state)
    }

    private fun saveRecord() {
       uiSystem.saveResult()
    }

    private fun gameEnd() {
        uiSystem.showEndDialog()
    }

}