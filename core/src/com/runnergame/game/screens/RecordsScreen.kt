package com.runnergame.game.screens

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.ScreenUtils
import com.runnergame.game.RunnerGame
import com.runnergame.game.assets.Assets
import com.runnergame.game.util.MonsterRow
import com.runnergame.game.util.Records

class RecordsScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createScreen()
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(RunnerGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createScreen() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.background = TextureRegionDrawable(TextureRegion(Assets.recordsBack))
        table.pad(20.0f)

        val tableWidth = Gdx.graphics.width - 140.0f
        val back = Container(table)
        back.background = skin.getDrawable("background")
        back.pad(20.0f, 50.0f, 20.0f, 50.0f)
        back.width(tableWidth)
        back.setFillParent(true)
        stage.addActor(back)

//        back.debug = true
//        table.debug = true

        table.add(Label("РЕКОРДЫ", skin, "records").apply { scaleBy(10.0f) }).expandX().center()
        val exitButton = ImageButton(skin.getDrawable("button_close"))
        exitButton.addListener( object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                game.screen = MenuScreen(game)
            }
        })
        table.add(exitButton).right()
        table.row()

        val recordsGroup = VerticalGroup()
        val scroll = ScrollPane(recordsGroup)
        scroll.setFlickScroll(true)
        recordsGroup.space(10.0f)

        table.center().add(scroll).expand().top().minHeight(10.0f)

        for(record in Records.getRecords()) {
            val recTable = Table()
            recTable.add(Label(record.points.toString(), skin,"records").apply { setAlignment(Align.center) }).top().pad(10.0f).center().width(tableWidth - 100)
            recTable.row()
            recTable.add(MonsterRow(record.killedMonsters.toList()))
                .center()
                .size((record.killedMonsters.size * 50.0f).coerceAtMost(tableWidth - 100).coerceAtLeast(100.0f), if(record.killedMonsters.size > 0) 100.0f else 15.0f)
            recordsGroup.addActor(recTable)
        }
    }
}