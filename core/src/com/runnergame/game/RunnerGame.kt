package com.runnergame.game

import com.badlogic.gdx.Game
import com.runnergame.game.assets.Assets
import com.runnergame.game.screens.MenuScreen

class RunnerGame: Game() {

	companion object {
		const val PERFECT_DT: Float = (16.67 * 2 / 1000).toFloat()
	}

	override fun create () {
		Assets.init()
		setScreen(MenuScreen(this))
	}

	override fun dispose () {
		Assets.dispose()
	}
}
