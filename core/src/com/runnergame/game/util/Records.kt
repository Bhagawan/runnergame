package com.runnergame.game.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Json

class Records {
    companion object {
        fun saveRecord(record: Record) {
            val prefs = Gdx.app.getPreferences("Records")
            val j = Json()
            var savedRecords = j.fromJson(Array<Record>::class.java, prefs.getString("records", "[]"))
            savedRecords = savedRecords.plusElement(record)
            savedRecords.sortByDescending { it.points }
            prefs.putString("records", j.toJson(savedRecords))
            prefs.flush()
        }

        fun getRecords(): Array<Record> {
            val prefs = Gdx.app.getPreferences("Records")
            val r = prefs.getString("records", "[]")
            return Json().fromJson(Array<Record>::class.java, r)
        }
    }
}