package com.runnergame.game.util

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.runnergame.game.assets.Assets

class MonsterRow(private val monsters: List<Obstacles>): Actor() {
    private val zombie = Assets.zombieAnim.getKeyFrame(0.0f)
    private val skeleton = Assets.skeletonAnim.getKeyFrame(0.0f)
    private val bat = Assets.batAnim.getKeyFrame(0.0f)
    private val grass :TiledDrawable

    init {
        val terrain = Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("terrain")
        val top = Sprite()
        top.setRegion(terrain, 13, 0, 22, 15)
        grass = TiledDrawable(top)
    }

    override fun draw(batch: Batch?, parentAlpha: Float) {
        val scale = (height - 15) / skeleton.regionHeight
        for(monster in monsters.withIndex()) {
            when(monster.value) {
                Obstacles.SKELETON -> {
                    batch?.draw(skeleton,x + ((width / monsters.size).coerceAtMost((skeleton.regionWidth * scale)) * monster.index),
                        y + 15,skeleton.regionWidth * scale , height - 15)
                }
                Obstacles.ZOMBIE -> {
                    batch?.draw(zombie, x + ((width / monsters.size).coerceAtMost(zombie.regionWidth * scale) * monster.index),
                        y + 15,zombie.regionWidth * scale , height - 15)
                }
                Obstacles.BAT -> {
                    batch?.draw(bat,x + ((width / monsters.size).coerceAtMost(bat.regionWidth * scale) * monster.index),
                        y + 15,bat.regionWidth * scale , height - 15)
                }
                else -> {}
            }
        }
        grass.draw(batch, x,y, width, 15.0f)

    }
}