package com.runnergame.game.util

data class Record(var points: Int, var killedMonsters: ArrayList<Obstacles>) {
    constructor(): this(0, ArrayList<Obstacles>())
    constructor(points: Int, killedMonsters: List<Obstacles>): this(points, ArrayList(killedMonsters))
}