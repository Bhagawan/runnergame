package com.runnergame.game.util

enum class Obstacles {
    WALL, ZOMBIE, SKELETON, BAT, GOBLIN, SAW
}