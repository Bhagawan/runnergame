package com.runnergame.game.assets

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.Array

class Assets {
    companion object {
        val assetManager = AssetManager()
        lateinit var heroRunAnim: Animation<TextureRegion>
        lateinit var heroJumpAnim: Animation<TextureRegion>
        lateinit var heroDeathAnim: Animation<TextureRegion>
        lateinit var batAnim: Animation<TextureRegion>
        lateinit var coinAnim: Animation<TextureRegion>
        lateinit var zombieAnim: Animation<TextureRegion>
        lateinit var skeletonAnim: Animation<TextureRegion>
        lateinit var shotAnim: Animation<TextureRegion>
        lateinit var goblinAnim: Animation<TextureRegion>

        lateinit var effectPool : ParticleEffectPool

        lateinit var recordsBack: Texture

        fun init() {
            assetManager.load("skin/skin.atlas", TextureAtlas::class.java)
            assetManager.load("skin/MazzardSoft.fnt", BitmapFont::class.java)
            assetManager.load("skin/skin.json", Skin::class.java)

            val partParameter = ParticleEffectLoader.ParticleEffectParameter()
            partParameter.atlasFile = "skin/skin.atlas"
            assetManager.load("explosion", ParticleEffect::class.java, partParameter)

            while(!assetManager.update()) { println("efefefefefefefefe")}
            effectPool = ParticleEffectPool((assetManager.get("explosion", ParticleEffect::class.java)), 1, 10)

            createAnimations()
            createRecordsTexture()
        }

        private fun createAnimations() {
            heroRunAnim = createAnim("Gunner_Run", 48, 40, 0.05f)
            heroJumpAnim = createAnim("Gunner_Jump", 48, 40, 0.9f)
            heroDeathAnim = createAnim("Gunner_Death", 48, 40, 0.1f)
            heroDeathAnim.playMode = Animation.PlayMode.NORMAL
            batAnim = createAnim("bat", 32, 32, 0.6f)
            zombieAnim = createAnim("zombie", 32, 50, 0.6f)
            skeletonAnim = createAnim("skeleton", 32, 50, 0.6f)
            coinAnim = createAnim("coin", 20, 20, 0.05f)
            shotAnim = createAnim("shot", 16, 16, 0.5f)
            shotAnim.playMode = Animation.PlayMode.NORMAL
            goblinAnim = createAnim("goblin", 24, 16, 0.6f)
        }

        private fun createAnim(stripName: String, frameWidth : Int, frameHeight: Int, duration: Float) : Animation<TextureRegion> {
            val animStrip = assetManager.get("skin/skin.json", Skin::class.java)
                .getRegion(stripName)
                .split(frameWidth, frameHeight)
            val animFrames = Array(Array<TextureRegion>(animStrip[0].size * animStrip.size) { t -> animStrip[t / animStrip[0].size][t % animStrip[0].size] })
            return Animation(duration, animFrames, Animation.PlayMode.LOOP)
        }

        private fun createRecordsTexture() {
            val skin = assetManager.get("skin/skin.json", Skin::class.java)
            val paper = skin.getRegion("menu_back")
            val textureData = paper.texture.textureData
            if (!textureData.isPrepared) textureData.prepare()
            val pix = textureData.consumePixmap()

            val blockSize = paper.regionWidth / 2
            val back = Pixmap(paper.regionWidth * 3, (paper.regionHeight * 1.5).toInt(), textureData.format)

            for(c in 0..6) {
                for(r in 0..2) {
                    val offX = when(c) {
                        0 -> 0
                        6 -> blockSize
                        else -> blockSize / 2
                    }
                    val offY = when(r) {
                        0 -> 0
                        2 -> blockSize
                        else -> blockSize / 2
                    }
                    back.drawPixmap(pix, blockSize * c,blockSize * r, paper.regionX + offX, paper.regionY + offY, blockSize, blockSize)
                }
            }


            recordsBack = Texture(back)
            back.dispose()
        }

        fun dispose() {
            assetManager.dispose()
            recordsBack.dispose()
        }
    }
}